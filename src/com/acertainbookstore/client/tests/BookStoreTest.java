package com.acertainbookstore.client.tests;

import static org.junit.Assert.*;

import java.util.*;

import com.acertainbookstore.business.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.acertainbookstore.client.BookStoreHTTPProxy;
import com.acertainbookstore.client.StockManagerHTTPProxy;
import com.acertainbookstore.interfaces.BookStore;
import com.acertainbookstore.interfaces.StockManager;
import com.acertainbookstore.utils.BookStoreConstants;
import com.acertainbookstore.utils.BookStoreException;

/**
 * {@link BookStoreTest} tests the {@link BookStore} interface.
 * 
 * @see BookStore
 */
public class BookStoreTest {

	/** The Constant TEST_ISBN. */
	private static final int TEST_ISBN = 3044560;

	/** The Constant NUM_COPIES. */
	private static final int NUM_COPIES = 5;
	

	/** The local test. */
	private static boolean localTest = true;

	/** The store manager. */
	private static StockManager storeManager;

	/** The client. */
	private static BookStore client;

	/**
	 * Sets the up before class.
	 */
	@BeforeClass
	public static void setUpBeforeClass() {
		try {
			String localTestProperty = System.getProperty(BookStoreConstants.PROPERTY_KEY_LOCAL_TEST);
			localTest = (localTestProperty != null) ? Boolean.parseBoolean(localTestProperty) : localTest;
			//localTest=false;
			if (localTest) {
				CertainBookStore store = new CertainBookStore();
				storeManager = store;
				client = store;
			} else {
				storeManager = new StockManagerHTTPProxy("http://localhost:8081/stock");
				client = new BookStoreHTTPProxy("http://localhost:8081");
			}

			storeManager.removeAllBooks();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Helper method to add some books.
	 *
	 * @param isbn
	 *            the isbn
	 * @param copies
	 *            the copies
	 * @throws BookStoreException
	 *             the book store exception
	 */
	public void addBooks(int isbn, int copies) throws BookStoreException {
		Set<StockBook> booksToAdd = new HashSet<StockBook>();
		StockBook book = new ImmutableStockBook(isbn, "Test of Thrones", "George RR Testin'", (float) 10, copies, 0, 0,
				0, false);
		booksToAdd.add(book);
		storeManager.addBooks(booksToAdd);
	}

	/**
	 * Helper method to get the default book used by initializeBooks.
	 *
	 * @return the default book
	 */
	public StockBook getDefaultBook() {
		return new ImmutableStockBook(TEST_ISBN, "Harry Potter and JUnit", "JK Unit", (float) 10, NUM_COPIES, 0, 0, 0,
				false);
	}

	/**
	 * Method to add a book, executed before every test case is run.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Before
	public void initializeBooks() throws BookStoreException {
		Set<StockBook> booksToAdd = new HashSet<StockBook>();
		booksToAdd.add(getDefaultBook());
		storeManager.addBooks(booksToAdd);
	}

	/**
	 * Method to clean up the book store, execute after every test case is run.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@After
	public void cleanupBooks() throws BookStoreException {
		storeManager.removeAllBooks();
	}


	/**
	 * Tests getBooksInDemand() functionality.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Test
	public void testGetBooksInDemand() throws BookStoreException {

		Set<StockBook> booksToAdd = new HashSet<StockBook>();

		ImmutableStockBook book1=new ImmutableStockBook(25000, "Title", "Author", 230, 1, 2,
				7, 30, false);
		ImmutableStockBook book2=new ImmutableStockBook(300, "Title2", "Autho2r", 230, 7, 0,
				7, 30, false);
		ImmutableStockBook book3=new ImmutableStockBook(800, "Title3", "Author3", 200, 1, 4,
				7, 30, false);

		booksToAdd.add(book1);
		booksToAdd.add(book2);
		booksToAdd.add(book3);
		storeManager.addBooks(booksToAdd);

		HashSet<BookCopy> booksToBuy = new HashSet<BookCopy>();

		booksToBuy.add(new BookCopy(25000, 1));
		booksToBuy.add(new BookCopy(300, 1));
		booksToBuy.add(new BookCopy(800, 1));
		client.buyBooks(booksToBuy);


		List<Integer> isbnsOfResultedBooks = new ArrayList<>();
		List<StockBook> actualResult = storeManager.getBooksInDemand();

		for (StockBook book: actualResult) {
			isbnsOfResultedBooks.add(book.getISBN());
		}

		isbnsOfResultedBooks.sort( (Integer i1, Integer i2) -> {
			if (i1 < i2) return -1;
			return 1;
		});

		assertTrue(isbnsOfResultedBooks.get(0) == 800 &&
				isbnsOfResultedBooks.get(1) == 25000);
	}

	/**
	 * Tests that getTopRatedBooks() fails when
	 * less then one books is requested.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Test
	public void testGetTopRatedBooksFails() throws BookStoreException {
		try {
			client.getTopRatedBooks(0);
			fail();
		} catch (BookStoreException ex) {
			;
		}

	}


	/**
	 * Tests getTopRatedBooks() functionality.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Test
	public void testGetTopRatedBooks() throws BookStoreException {
		HashSet<Integer> isbns= new HashSet<Integer>();

		HashSet<BookRating> bookRatings=new HashSet<BookRating>();

		addBooks(23000, 2);
		isbns.add(23000);
		BookRating rating=new BookRating(23000,5);
		bookRatings.add(rating);

		addBooks(300, 10);
		isbns.add(300);
		BookRating rating2=new BookRating(300,1);
		bookRatings.add(rating2);

		addBooks(10000, 5);
		isbns.add(10000);
		BookRating rating3=new BookRating(10000,3);
		bookRatings.add(rating3);

		client.rateBooks(bookRatings);

		List<StockBook> books = storeManager.getBooksByISBN(isbns);

//		need ImmutableBook objects
		List<Book> actualResult = client.getTopRatedBooks(2);

		assertTrue(actualResult.get(0).getISBN() == 23000 &&
							actualResult.get(1).getISBN() == 10000);
	}


	/**
	 * Tests basic buyBook() functionality.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Test
	public void testBuyAllCopiesDefaultBook() throws BookStoreException {
		// Set of books to buy
		Set<BookCopy> booksToBuy = new HashSet<BookCopy>();
		booksToBuy.add(new BookCopy(TEST_ISBN, NUM_COPIES));

		// Try to buy books
		client.buyBooks(booksToBuy);

		List<StockBook> listBooks = storeManager.getBooks();
		assertTrue(listBooks.size() == 1);
		StockBook bookInList = listBooks.get(0);
		StockBook addedBook = getDefaultBook();

		assertTrue(bookInList.getISBN() == addedBook.getISBN() && bookInList.getTitle().equals(addedBook.getTitle())
				&& bookInList.getAuthor().equals(addedBook.getAuthor()) && bookInList.getPrice() == addedBook.getPrice()
				&& bookInList.getNumSaleMisses() == addedBook.getNumSaleMisses()
				&& bookInList.getAverageRating() == addedBook.getAverageRating()
				&& bookInList.getNumTimesRated() == addedBook.getNumTimesRated()
				&& bookInList.getTotalRating() == addedBook.getTotalRating()
				&& bookInList.isEditorPick() == addedBook.isEditorPick());
	}

	/**
	 * Tests that books with invalid ISBNs cannot be bought.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Test
	public void testBuyInvalidISBN() throws BookStoreException {
		List<StockBook> booksInStorePreTest = storeManager.getBooks();

		// Try to buy a book with invalid ISBN.
		HashSet<BookCopy> booksToBuy = new HashSet<BookCopy>();
		booksToBuy.add(new BookCopy(TEST_ISBN, 1)); // valid
		booksToBuy.add(new BookCopy(-1, 1)); // invalid

		// Try to buy the books.
		try {
			client.buyBooks(booksToBuy);
			fail();
		} catch (BookStoreException ex) {
			;
		}

		List<StockBook> booksInStorePostTest = storeManager.getBooks();

		// Check pre and post state are same.
		assertTrue(booksInStorePreTest.containsAll(booksInStorePostTest)
				&& booksInStorePreTest.size() == booksInStorePostTest.size());
	}

	/**
	 * Tests that books can only be bought if they are in the book store.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Test
	public void testBuyNonExistingISBN() throws BookStoreException {
		List<StockBook> booksInStorePreTest = storeManager.getBooks();

		// Try to buy a book with ISBN which does not exist.
		HashSet<BookCopy> booksToBuy = new HashSet<BookCopy>();
		booksToBuy.add(new BookCopy(TEST_ISBN, 1)); // valid
		booksToBuy.add(new BookCopy(100000, 10)); // invalid

		// Try to buy the books.
		try {
			client.buyBooks(booksToBuy);
			fail();
		} catch (BookStoreException ex) {
			;
		}

		List<StockBook> booksInStorePostTest = storeManager.getBooks();

		// Check pre and post state are same.
		assertTrue(booksInStorePreTest.containsAll(booksInStorePostTest)
				&& booksInStorePreTest.size() == booksInStorePostTest.size());
	}
	
	/**
	 * Tests that books can be rated only with a rating between 0 and 5.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */	
	@Test
	public void testRateBooksInvalidScore() throws BookStoreException{
		HashSet<BookRating> bookRatings=new HashSet<BookRating>();
		BookRating rating=new BookRating(TEST_ISBN,5);
		bookRatings.add(rating);
		rating=new BookRating(TEST_ISBN+100,3);
		bookRatings.add(rating);
		rating=new BookRating(TEST_ISBN+200,-5);
		bookRatings.add(rating);
		rating=new BookRating(TEST_ISBN+300,4);
		bookRatings.add(rating);
		
		try {
			HashSet<Integer> isbns= new HashSet<Integer>();
			isbns.add(TEST_ISBN);
			isbns.add(TEST_ISBN+100);
			isbns.add(TEST_ISBN+200);
			isbns.add(TEST_ISBN+300);
			
			List<StockBook> books = storeManager.getBooksByISBN(isbns);
			StockBook test_book=books.get(0);

			
			long actual_rating=test_book.getTotalRating();
			long num_times_rated=test_book.getNumTimesRated();
			
			client.rateBooks(bookRatings);
			
			books = storeManager.getBooksByISBN(isbns);
			test_book=books.get(0);
			assertTrue((actual_rating == test_book.getTotalRating()) 
					&& (num_times_rated == test_book.getNumTimesRated()));
			
		}catch(BookStoreException e) {
				;
		}
	}

	/**
	 * Tests that the rating takes place.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */	
	@Test
	public void testRateBooksValid() throws BookStoreException{
		final int five = 5;
		
		HashSet<BookRating> bookRatings=new HashSet<BookRating>();
		BookRating rating=new BookRating(TEST_ISBN,five);
		bookRatings.add(rating);

		rating=new BookRating(TEST_ISBN+100,five);
		bookRatings.add(rating);
		
		addBooks(TEST_ISBN+100,10);
		try {
			HashSet<Integer> isbns= new HashSet<Integer>();
			isbns.add(TEST_ISBN);
			isbns.add(TEST_ISBN+100);
			
			List<StockBook> books = storeManager.getBooksByISBN(isbns);
			StockBook test_book=books.get(0);
			StockBook test_book_2=books.get(1);
			
			long actual_rating=test_book.getTotalRating();
			long num_times_rated=test_book.getNumTimesRated();
			
			long actual_rating_2=test_book.getTotalRating();
			long num_times_rated_2=test_book.getNumTimesRated();			
			
			client.rateBooks(bookRatings);
			
			books = storeManager.getBooksByISBN(isbns);
			test_book=books.get(0);
			test_book_2=books.get(1);
			assertTrue((actual_rating + five== test_book.getTotalRating()) 
					&& (num_times_rated+1 == test_book.getNumTimesRated())
					&& (actual_rating_2 + five== test_book_2.getTotalRating()) 
					&& (num_times_rated_2+1 == test_book_2.getNumTimesRated()));
			
		}catch(Exception e) {
			System.out.println(e.getMessage()+" could not get book with the test isbn");
		}
		
	}
	
	
	/**
	 * Tests that you can't buy more books than there are copies.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Test
	public void testBuyTooManyBooks() throws BookStoreException {
		List<StockBook> booksInStorePreTest = storeManager.getBooks();

		// Try to buy more copies than there are in store.
		HashSet<BookCopy> booksToBuy = new HashSet<BookCopy>();
		booksToBuy.add(new BookCopy(TEST_ISBN, NUM_COPIES + 1));

		try {
			client.buyBooks(booksToBuy);
			fail();
		} catch (BookStoreException ex) {
			;
		}

		List<StockBook> booksInStorePostTest = storeManager.getBooks();
		assertTrue(booksInStorePreTest.containsAll(booksInStorePostTest)
				&& booksInStorePreTest.size() == booksInStorePostTest.size());
	}

	/**
	 * Tests that you can't buy a negative number of books.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Test
	public void testBuyNegativeNumberOfBookCopies() throws BookStoreException {
		List<StockBook> booksInStorePreTest = storeManager.getBooks();

		// Try to buy a negative number of copies.
		HashSet<BookCopy> booksToBuy = new HashSet<BookCopy>();
		booksToBuy.add(new BookCopy(TEST_ISBN, -1));

		try {
			client.buyBooks(booksToBuy);
			fail();
		} catch (BookStoreException ex) {
			;
		}

		List<StockBook> booksInStorePostTest = storeManager.getBooks();
		assertTrue(booksInStorePreTest.containsAll(booksInStorePostTest)
				&& booksInStorePreTest.size() == booksInStorePostTest.size());
	}

	/**
	 * Tests that all books can be retrieved.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Test
	public void testGetBooks() throws BookStoreException {
		Set<StockBook> booksAdded = new HashSet<StockBook>();
		booksAdded.add(getDefaultBook());

		Set<StockBook> booksToAdd = new HashSet<StockBook>();
		booksToAdd.add(new ImmutableStockBook(TEST_ISBN + 1, "The Art of Computer Programming", "Donald Knuth",
				(float) 300, NUM_COPIES, 0, 0, 0, false));
		booksToAdd.add(new ImmutableStockBook(TEST_ISBN + 2, "The C Programming Language",
				"Dennis Ritchie and Brian Kerninghan", (float) 50, NUM_COPIES, 0, 0, 0, false));

		booksAdded.addAll(booksToAdd);

		storeManager.addBooks(booksToAdd);

		// Get books in store.
		List<StockBook> listBooks = storeManager.getBooks();

		// Make sure the lists equal each other.
		assertTrue(listBooks.containsAll(booksAdded) && listBooks.size() == booksAdded.size());
	}

	/**
	 * Tests that a list of books with a certain feature can be retrieved.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Test
	public void testGetCertainBooks() throws BookStoreException {
		Set<StockBook> booksToAdd = new HashSet<StockBook>();
		booksToAdd.add(new ImmutableStockBook(TEST_ISBN + 1, "The Art of Computer Programming", "Donald Knuth",
				(float) 300, NUM_COPIES, 0, 0, 0, false));
		booksToAdd.add(new ImmutableStockBook(TEST_ISBN + 2, "The C Programming Language",
				"Dennis Ritchie and Brian Kerninghan", (float) 50, NUM_COPIES, 0, 0, 0, false));

		storeManager.addBooks(booksToAdd);

		// Get a list of ISBNs to retrieved.
		Set<Integer> isbnList = new HashSet<Integer>();
		isbnList.add(TEST_ISBN + 1);
		isbnList.add(TEST_ISBN + 2);

		// Get books with that ISBN.
		List<Book> books = client.getBooks(isbnList);

		// Make sure the lists equal each other
		assertTrue(books.containsAll(booksToAdd) && books.size() == booksToAdd.size());
	}

	/**
	 * Tests that books cannot be retrieved if ISBN is invalid.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@Test
	public void testGetInvalidIsbn() throws BookStoreException {
		List<StockBook> booksInStorePreTest = storeManager.getBooks();

		// Make an invalid ISBN.
		HashSet<Integer> isbnList = new HashSet<Integer>();
		isbnList.add(TEST_ISBN); // valid
		isbnList.add(-1); // invalid

		HashSet<BookCopy> booksToBuy = new HashSet<BookCopy>();
		booksToBuy.add(new BookCopy(TEST_ISBN, -1));

		try {
			client.getBooks(isbnList);
			fail();
		} catch (BookStoreException ex) {
			;
		}

		List<StockBook> booksInStorePostTest = storeManager.getBooks();
		assertTrue(booksInStorePreTest.containsAll(booksInStorePostTest)
				&& booksInStorePreTest.size() == booksInStorePostTest.size());
	}

	/**
	 * Tear down after class.
	 *
	 * @throws BookStoreException
	 *             the book store exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws BookStoreException {
		storeManager.removeAllBooks();

		if (!localTest) {
			((BookStoreHTTPProxy) client).stop();
			((StockManagerHTTPProxy) storeManager).stop();
		}
	}
}
